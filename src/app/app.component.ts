import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'CodeSandbox';
  vetor = [10, 20, 30];

  /**
   * Esse método auxilia o *ngFor a detectar
   * mudanças, evitando que todo o DOM seja
   * renderizado desnecessariamente.
   *
   * @param index índice do vetor, objeto, etc
   * @param obj valor atual do vetor, objeto, etc.
   */
  trackByIndex(index: number, obj: any) {
    return index;
  }
}
